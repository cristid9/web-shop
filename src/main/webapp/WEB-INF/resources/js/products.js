$(document).ready(function () {
    ProductsListenersComponent.bindAll();
    console.log("Hello from here");
});

ProductsListenersComponent = (function() {

    var bindAllComponents = function() {
        console.log("New interceptor binding");
        bindClickIntercept();
    };

    var bindClickIntercept = function() {
        $(".category_filter_link").on('click', function(target) {
            var category = $(target.target).text();
            $("#hidden_category_marker").val(category.trim());
            $("#hidden_form").submit();
        });
    };

    return {
        bindAll: bindAllComponents
    };
})();