$(document).ready(function() {
    ProductRemoverComponent.bindAll();
});

ProductRemoverComponent = (function () {
    var bindAllComponents = function () {
        handleRemove();
    };

    var handleRemove = function () {
        $('.remove_product').on('click', function (e) {
            var removeId = $(e.target).siblings('.id_value').val();

            // remove div with product
            $('#product_' + removeId).remove();

            // recompute price
            OrderPriceComponent.updatePriceLabel();

            // tell change to server
            $.post("remove_product", {target_id: removeId}, function (response) {
            }).done(function () {
                bootbox.alert({
                    size: "small",
                    title: "We're sorry!",
                    message: "Product removed!",
                    callback: function(){ }
                });
            });

            DataManager.amountProductsInCart--;

            if (DataManager.amountProductsInCart == 0) {
                // here we decide if we display the order now button or not
                $("#order_now_btn").prop('disabled', true);
            } else {
                $("#order_now_btn").prop('disabled', false);
            };
        });
    };

    return {
        bindAll: bindAllComponents,
    };
})();