$(document).ready(function () {
    OrderNowComponent.bindAll();
});

OrderNowComponent = (function () {
    var orderNowHandler = function () {

        if (DataManager.amountProductsInCart == 0) {
            // here we decide if we display the order now button or not

            $("#order_now_btn").prop('disabled', true);
        } else {
            $("#order_now_btn").prop('disabled', false);
        };

        $("#order_now_btn").on('click', function () {
            var total = OrderPriceComponent.calculateOrderTotal();

            if (total == 0) {
                return;
            }

            $.post("submit_order", $.param({}), function (response) {
            }).done(function(data) {
                var w = window.open('', '', 'width=200,height=200');
                w.document.write("We've received your order! You will soon be redirected to another page!");
                w.focus();

                setTimeout(function () {
                    window.location.href = "/mywebapp/orders";
                    w.close();
                }, 3000);
            });
        });

        // if cart is empty grey out the button
    };

    return {
        bindAll: orderNowHandler
    };
})();