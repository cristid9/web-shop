$(document).ready(function() {
    LoginComponentHandlers.bindAll();
    $("#empty-username").hide();
    $("#empty-password").hide();

    $("#login_button").on('click', function() {
        $("#login_form").submit();
    });
});

var is_valid_login_form = function() {
    var username = $("#username").val();
    var password = $("#password").val();
    var valid = true;

    if (!username) {
        valid = false;
        $("#empty-username").show();
    }

    if (!password) {
        valid = false;
        $("#empty-password").show();
    }

    return valid;
};

LoginComponentHandlers = (function() {

    var bindElements = function() {
        bindRedirectRegister();
    };

    var bindRedirectRegister = function () {
        $("#redirect_register_from_login").on("click", function() {
            window.location = "/mywebapp/register";
        });
    };

    return {
        bindAll: bindElements,
    };
})();