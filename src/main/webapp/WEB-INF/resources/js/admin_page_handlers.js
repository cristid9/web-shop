$(document).ready(function () {
    AdminHandlerComponent.performReq();
    AdminHandlerComponent.bindAll();
});

AdminHandlerComponent = (function() {
    var populateUsersTarget = function() {
        $.get("list_users", {}, function () {


        }).done(function (result) {
            result = JSON.parse(result);
            console.log(result);
            $("#delete_user_area_buttons").html("");
            for (var i = 0; i < result.length; ++i) {

                var strng = "<div class=\"list-group-item list-group-item-action\">" +
                    result[i].username;

                if (result[i].role != "admin") {
                    strng += "<button type=\"button\" class=\"btn btn-success promote_admin_button\"> promote </button>";
                } else {
                    strng += "<button type=\"button\" class=\"btn btn-danger downgrade_user_button\">downgrade</button>";
                }

                strng += "   <button type=\"button\" class=\"btn btn-danger remove_user_button\">remove</button>" +
                    "   <input type=\"hidden\" value=\"" + result[i].id + "\"  class=\"remove_user_id\" />" +
                    "</div>";

                $("#delete_user_area_buttons").append(strng);
            }
        });
    };

    var populateCategoriesTarget = function() {
        $.get("list_categories", {}, function() {
        }).done(function(result) {
            result = JSON.parse(result);
            $("#delete_category_area_buttons").html("");
            for (var i = 0; i < result.length; ++i) {

                var strng = "<div class=\"list-group-item list-group-item-action\">" +
                    "   <code class=\"remove_category_info_id\"> " + result[i].id + " </code>" +
                    result[i].name +
                    "   <button type=\"button\" class=\"btn btn-danger remove_category_button\">remove</button>" +
                    "   <input type=\"hidden\" value=\"" + result[i].id + "\"  class=\"remove_category_id\" />" +
                    "</div>";

                $("#delete_category_area_buttons").append(strng);
            }
        });
    };

    var bindElements = function() {
        bindAddProduct();
        bindDeleteUser();
        bindDeleteCategory();
        bindMisc();
        bindChangeCurrency();
        bindManageOrders();
    };

    var bindDeleteCategory = function() {
        $("#delete_category").on("click", function () {
            $("#delete_user_area").hide();
            $("#add_product_area").hide();
            $("#manage_orders_area").hide();
            $("#misc_options_area").hide();
            $("#delete_category_area").show();
            populateCategoriesTarget();
        });

        $("#delete_category_area_buttons").on("click", ".remove_category_button", function () {
            var id = parseInt($(event.target).siblings('.remove_category_id').val());
            $.post("delete_category", {id: id}, function() {
            }).done(function(result) {
                populateCategoriesTarget();
            });
        });
    };

    var bindAddProduct = function() {
        $("#add_product").on('click', function() {
            $("#delete_user_area").hide();
            $("#delete_category_area").hide();
            $("#manage_orders_area").hide();
            $("#misc_options_area").hide();
            $("#add_product_area").show();
        });

        $("#proceed_add_new_product").on("click", function() {
            var name = $("#new_product_name").val();
            var producer = $("#new_product_producer").val();
            var price = $("#new_product_price").val();
            var category = $("#new_product_category").val();

            $.post("add_product", {
                name: name,
                producer: producer,
                price: price,
                category: category
            }, function() {
            }).done(function(result) {
                $("#new_product_name").val("");
                $("#new_product_producer").val("");
                $("#new_product_price").val("");
                $("#new_product_category").val("");
                alert("Product inserted successfully! You can add another one!");
            });
        });
    };

    var bindDeleteUser = function() {
        $("#delete_user").on('click', function() {
            $("#delete_user_area").show();
            $("#add_product_area").hide();
            $("#delete_category_area").hide();
            $("#misc_options_area").hide();
            $("#manage_orders_area").hide();

            populateUsersTarget();
        });

        $("#delete_user_area_buttons").on("click", ".remove_user_button", function (event) {
            var id = parseInt($(event.target).siblings('.remove_user_id').val());
            $.post("delete_user", {id: id}, function () {
            }).done(function (result) {
                populateUsersTarget();
            });
        });

        $("#delete_user_area_buttons").on("click", ".promote_admin_button", function(event) {
            var id = parseInt($(event.target).siblings('.remove_user_id').val());
            $.post("promote_user", {id: id}, function() {
            }).done(function (result) {
                console.log("user promoted to admin");
                populateUsersTarget();
            });
        });

        $("#delete_user_area_buttons").on("click", ".downgrade_user_button", function (event) {
            var id = parseInt($(event.target).siblings('.remove_user_id').val());
            $.post("downgrade_user", {id: id}, function () {
            }).done(function (result) {
               populateUsersTarget();
            });
        });

    };

    var performRequirements = function () {
        $("#delete_user_area").hide();
        $("#add_product_area").hide();
        $("#misc_options_area").hide();
        $("#manage_orders_area").hide();
        $("#delete_category_area").hide();
    };

    var bindMisc = function() {
        $("#misc").on("click", function() {
            $("#delete_user_area").hide();
            $("#add_product_area").hide();
            $("#delete_category_area").hide();
            $("#manage_orders_area").hide();
            $("#misc_options_area").show();
        });
    };

    var bindChangeCurrency = function () {
        $("#change_currency").on("click", function () {
            $.post("change_currency", {currency: $("#currency").val()}, function () {
            }).done(function (result) {
                bootbox.alert({
                    size: "small",
                    title: "Neat!",
                    message: "Currency changed!",
                    callback: function(){ }
                });
            });
        });
    };

    var bindManageOrders = function () {
        $("#delete_user_area").hide();
        $("#add_product_area").hide();
        $("#delete_category_area").hide();
        $("#misc_options_area").hide();
        $("#manage_orders_area").show();
    };

    return {
        bindAll: bindElements,
        performReq: performRequirements,
    };
})();