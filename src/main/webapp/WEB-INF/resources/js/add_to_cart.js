$(document).ready(function() {
   $(".possible_cart_product").on('click', function(event) {
        var id = $(this).siblings().val();

        $.post("add_to_cart", $.param({product_id: id}),  function(response) {
        })
        .done(function(data) {
            DataManager.amountProductsInCart++;
            console.debug("  >> " + DataManager.amountProductsInCart + " <<  ");
            bootbox.alert({
                size: "small",
                title: "Well done!",
                message: "The product was successfully added to your cart!",
                callback: function(){ }
            });
        });
   })
});