$(document).ready(function () {
    CategoryAdderComponent.bindAll();
});

CategoryAdderComponent = (function () {
    var bindElements = function () {
        bindAddButton();
    };

    var bindAddButton = function() {
        $("#add_category_button").on('click', function () {
            var newCategoryName = $("#add_category_input").val();
            console.log("Adding a new category with the name: " + newCategoryName);
            $.post("add_category", {name: newCategoryName}, function () {
            }).done(function (data) {
                data = JSON.parse(data);

                if (data.status == "OK") {
                    location.reload();
                } else {
                    alert("Could not add the desired category!");
                }
            });
        });
    };

    return {
        bindAll: bindElements
    };

})();