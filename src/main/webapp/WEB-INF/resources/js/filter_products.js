$(document).ready(function() {

});

CategoryListenerComponent = (function() {

    var bindAllComponents = function() {
        bindIncrement();
    };

    var bindIncrement = function() {

        $('.quantity-right-plus').click(function(e) {
            e.preventDefault();
            var targetElement = $(e.target).parent().siblings("input.quantity");
            var quantity = parseInt(targetElement.val());
            targetElement.val(quantity + 1);
            OrderPriceComponent.updatePriceLabel();
        });

        $('.quantity-left-minus').click(function(e) {
            e.preventDefault();
            var targetElement = $(e.target).parent().siblings("input.quantity");
            var quantity = parseInt(targetElement.val());

            if(quantity > 0) {
                targetElement.val(quantity - 1);
                OrderPriceComponent.updatePriceLabel();
            }
        });
    };

    return {
        bindAll: bindAllComponents
    };
})();