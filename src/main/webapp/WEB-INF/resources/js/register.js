$(document).ready(function() {
    RegisterComponentHandlers.bindAll();


    $("#primary_address_info").hide();
    $("#invalid-phone-number").hide();
    $("#invalid-username").hide();
    $("#invalid-password-1").hide();
    $("#invalid-password-2").hide();
    $("#no-match-password-1").hide();
    $("#no-match-password-2").hide();
    $("#hide_address_info").hide();

    $("#add_address_info").on('click', function() {
        $("#primary_address_info").show();
        $("#add_address_info").hide();
        $("#hide_address_info").show();
    });

    $("#submit_register").on('click', function() {
       if (is_valid_basic_info()) {
            $("#register_form").submit();
       }
    });

    $("#hide_address_info").on("click", function () {
        $("#primary_address_info").hide();
        $("#add_address_info").show();
        $("#hide_address_info").hide();
    });
});

var is_valid_basic_info = function() {
    var password1 = $("#password1").val();
    var password2 = $("#password2").val();
    var username = $("#username").val();
    var phoneNumber = $("#phoneNumber").val();

    var valid = true;

    if (!phoneNumber || phoneNumber.length < 8) {
        $("#invalid-phone-number").show();
        $("#phoneNumber").addClass("is-invalid");
        valid = false;

        console.log(phoneNumber);
    }

    if (!username || username.length < 8) {
        $("#invalid-username").show();
        $("#username").addClass("is-invalid");
        valid = false;
    }

    if (!password1 || password1.length < 8) {
        $("#invalid-password-1").show();
        $("#password1").addClass("is-invalid");

        valid = false;
    }

    if (!password2 || password2.length < 8) {
        $("invalid-password-2").show();
        $("#password2").addClass("is-invalid");

        valid = false;
    }

    if (password1 != password2) {
        $("#invalid-password-1").hide();
        $("#invalid-password-2").hide();

        $("#no-match-password-1").show();
        $("#no-match-password-2").show();

        valid = false;
    }

    if (valid) {
        console.log("The form is valid");
    }

    return valid;
};


RegisterComponentHandlers = (function() {
    var bindElements = function () {
        bindRedirectToLoginPage();
    };

    var bindRedirectToLoginPage = function() {
        $("#redirect_to_login_page_from_register").on("click", function () {
            window.location = "/mywebapp/login";
        });
    };

    return {
        bindAll: bindElements,
    };
})();
