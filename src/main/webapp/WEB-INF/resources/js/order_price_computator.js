OrderPriceComponent = (function () {
    var calculateOrderTotal = function () {
        var orderValue = 0;
        var metadatas = $(".crawled_area");

        for (var i = 0; i < metadatas.length; ++i) {
            var quantity = parseInt($(metadatas[i]).find(".quantity").val());
            var price = parseInt($(metadatas[i]).find(".product_price").val());

            if (quantity < 0) {
                alert("Cantitate invalida!! Resetata automat la 1");
                quantity = 1;
                $(metadatas[i]).find(".quantity").val(1);
            }

            orderValue += quantity * price;
        }

        return orderValue;
    };

    var updatePriceLabel = function() {
        $("#order_amount").text(calculateOrderTotal());
    };

    return {
        calculateOrderTotal: calculateOrderTotal,
        updatePriceLabel: updatePriceLabel
    };
})();
