var DataManager = {
    amountProductsInCart: null,
};

var populateDataManager = (function() {
    // load number of products in cart
    $.post('get_products_amount', {}, function () {
    }).done(function (data) {

        console.log('[PRODUCTS AMOUNT] SUCCESSFULLY LOADED AMOUNT OF PRODUCTS!');

        var data = JSON.parse(data);
        DataManager.amountProductsInCart = parseInt(data.amount);

    });
})();
