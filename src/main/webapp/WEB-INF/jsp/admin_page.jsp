<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>
            Admin Page
        </title>

        <%@include file='commons/libs_includes.jsp' %>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/cart_page.css" />" />
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/admin_page_style.css" />" />
        <script type="text/javascript" src="<c:url value="/resources/js/admin_page_handlers.js" />"></script>
    </head>
    <body>
        <%@include file="commons/header.jsp"%>
        <nav class="navbar navbar-default sidebar" role="navigation" id="admin_nav">
            <div class="container-fluid">
                <div class="navbar-header">
                </div>
                <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <center> == Admin options == </center>
                        </li>
                        <li id="delete_user">
                            <a href="#">
                                Manage users <span class="glyphicon glyphicon-list-alt pull-right"></span>
                            </a>
                        </li>
                        <li id="add_product">
                            <a href="#">
                                Add product <span class="glyphicon glyphicon-pencil pull-right"></span>
                            </a>
                        </li>
                        <li id="delete_category">
                            <a href="#">
                                Manage categories <span class="glyphicon glyphicon-folder-open pull-right"></span>
                            </a>
                        </li>
                        <li id="misc">
                            <a href="#">
                                Settings <span class="glyphicon glyphicon-cog pull-right"></span>
                            </a>
                        </li>
                        <li id="manage_orders">
                            <a href="#">
                                Manage Orders <span class="glyphicon glyphicon-wrench pull-right"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="main_content">
            <div id="manage_orders_area">
                <div class="list-group" id="manage_users_area_buttons">
                    CANNOT MANAGE ORDERS IN QUARANTINE MODE
                </div>
            </div>
            <div id="delete_user_area">
                <div class="list-group" id="delete_user_area_buttons">
                </div>
            </div>
            <div id="delete_category_area">
                <div class="list-group" id="delete_category_area_buttons">
                </div>
            </div>
            <div id="misc_options_area">
                <div class="form-group">
                    <label for="currency">Currency</label>
                    <input type="text" class="form-control" id="currency" placeholder="RON" style="width:200px" >
                </div>
                <button type="button" class="btn btn-light" id="change_currency">
                    Save settings
                </button>
            </div>
            <div id="add_product_area">
                <form>
                    <div class="form-group">
                        <label for="new_product_name">Name</label>
                        <input type="email" class="form-control" id="new_product_name" placeholder="Name"
                               style="width:200px">
                    </div>
                    <div class="form-group">
                        <label for="new_product_producer">Producer</label>
                        <input type="text" class="form-control" id="new_product_producer" placeholder="producer"
                               style="width:200px">
                    </div>
                    <div class="form-group">
                        <label for="new_product_price">Price</label>
                        <input type="text" class="form-control" id="new_product_price" placeholder="Price"
                               style="width:100px">
                    </div>
                    <div class="form-group">
                        <label for="new_product_category">Category</label>
                        <input type="text" class="form-control" id="new_product_category"
                               style="width:100px">
                    </div>
                    <button type="button" class="btn btn-primary" id="proceed_add_new_product">
                        Add
                    </button>
                </form>
            </div>
        </div>

        <%@include file="commons/footer.jsp"%>
    </body>
</html>
