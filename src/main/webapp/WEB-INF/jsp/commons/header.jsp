<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="containerDiv">
    <nav id="navMain" class="navbar">
        <div class="navbar-header">
            <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="" class="navbar-brand">YCShop </a>
        </div>

        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav" id="navbarUl">
                <li class="active">
                    <a href="/mywebapp/home">
                        <span class="glyphicon glyphicon-home"></span> Home
                    </a>
                </li>

                <c:choose>
                    <c:when test="${requestScope.logged_in != true}">
                        <li class="active">
                            <a href="/mywebapp/login">
                                <span class="glyphicon glyphicon-user"></span> login
                            </a>
                        </li>
                    </c:when>
                </c:choose>
                <c:choose>
                    <c:when test="${requestScope.logged_in != true}">
                        <li class="active">
                            <a href="/mywebapp/register">
                                <span class="glyphicon glyphicon-pencil"></span> register
                            </a>
                        </li>
                    </c:when>
                </c:choose>

                <li class="active">
                    <a href="/mywebapp/products">
                        <span class="glyphicon glyphicon-list-alt"></span> products
                    </a>
                </li>

                <c:choose>
                    <c:when test="${requestScope.logged_in != true}">
                        <li class="active" style="display: inline-block; vertical-align: middle;">
                            <a href="/mywebapp/cart">
                                <span class="glyphicon glyphicon-shopping-cart"></span>
                                cart page
                            </a>
                        </li>
                    </c:when>
                    <c:when test="${requestScope.logged_in == true}">
                        <li class="active">
                            <a href="/mywebapp/cart">
                                <span class="glyphicon glyphicon-shopping-cart"></span>
                                cart page
                            </a>
                        </li>
                    </c:when>
                </c:choose>

                <c:choose>
                    <c:when test="${requestScope.logged_in == true}">
                        <li class="active">
                            <a href="/mywebapp/orders">
                                <span class="glyphicon glyphicon-list"></span> my orders
                            </a>
                        </li>
                    </c:when>
                </c:choose>

                <c:choose>
                    <c:when test="${requestScope.logged_in == true}">
                        <li class="active">
                            <a href="/mywebapp/admin_page">
                                <span class="glyphicon glyphicon-th"></span> admin page
                            </a>
                        </li>
                    </c:when>
                </c:choose>
            </ul>

            <ul class="nav navbar-right">
                <c:choose>
                    <c:when test="${requestScope.logged_in == true}">
                        <li>
                            <a href="/mywebapp/logout">
                                <span class="glyphicon glyphicon-log-out"></span> Logout
                            </a>
                        </li>
                    </c:when>
                    <c:when test="${requestScope.logged_in == false}">
                        <li><a href="/mywebapp/login">Login</a></li>
                    </c:when>
                </c:choose>
            </ul>
        </div>
    </nav>
</div>


