<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
    <head>
        <title> Cart page </title>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/cart_page.css" />" />
        <%@include file='commons/libs_includes.jsp' %>
    </head>
    <body>
        <%@include file='commons/header.jsp' %>
        <script type="text/javascript" src="<c:url value="/resources/js/increment_quantity.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/order_price_computator.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/order_now_handlers.js" />"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/remove_product_from_cart.js" />"></script>

        <script type="text/javascript">
            OrderPriceComponent.updatePriceLabel();
            $(document).ready(function() {
                $(".quantity").on('change', function() {
                    OrderPriceComponent.updatePriceLabel();
                });
                OrderPriceComponent.updatePriceLabel();
                setTimeout(function() {
                    if (DataManager.amountProductsInCart == 0) {
                        // here we decide if we display the order now button or not
                        $("#order_now_btn").prop('disabled', true);
                    } else {
                        $("#order_now_btn").prop('disabled', false);
                    };
                }, 500);
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $.get("change_currency", {}, function () {

                }).done(function (data) {
                    $(".product_price").each(function () {
                        var original = $(this).text() + " " + data;
                        $(this).text(original);
                    });
                });

            });
        </script>

        <h1 id="cart_page_title">
            The products in your cart
            <span class="glyphicon glyphicon-shopping-cart"></span>
        </h1>

        <div class="list-group" id="cart_content_area">

            <c:forEach var="product"  items="${requestScope.current_cart.products}">
                <div class="list-group-item list-group-item-action flex-column align-items-start"
                    id="product_${product.product.id}">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"> ${product.product.name} </h5>
                        <small class="product_price"> ${product.product.price} </small>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-2">
                                <img src="https://i.qwerby.com/2018/07/img-placeholder.png"
                                     width="100px" height="50px" />
                            </div>
                            <div class="col-lg-3">
                                <span> Made by: </span>
                                <b> ${requestScope.producers[product.product.producer].name} </b>
                            </div>
                            <div class="col-lg-3">
                                <div class="input-group crawled_area">
                                    <button type="button" class="btn btn-danger btn-number quantity-left-minus"
                                            data-type="minus" data-field="">
                                        <span class="glyphicon glyphicon-minus quantity-left-minus"></span>
                                    </button>
                                    <input type="text" class="quantity" class="form-control input-sm"
                                           value="${product.quantity}"
                                           min="1" max="100" maxlength="3" />
                                    <input type="hidden" class="product_price" value="${product.price}" />
                                    <button type="button" class="btn btn-success btn-number quantity-right-plus"
                                            data-type="plus"
                                            data-field="">
                                        <span class="glyphicon glyphicon-plus quantity-right-plus"></span>
                                    </button>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <input type="hidden" class="id_value" value="${product.product.id}" />
                                <span class="glyphicon glyphicon-remove remove_product"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
        <br />

        <c:choose>
            <c:when test="${requestScope.logged_in eq true}">
                <div class="list-group" id="cart_content_area">
                    <div class="list-group-item list-group-item-action flex-column align-items-start ">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-4">
                                    <h2> Total amount </h2>
                                </div>
                                <div class="col-lg-2">
                                    <h2 id="order_amount"> 0 </h2>
                                </div>
                                <div class="col-lg-2">
                                    <button type="button" id="order_now_btn" class="btn btn-success">
                                        Order now
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
        </c:choose>
        <%@include file="commons/footer.jsp"%>
    </body>
</html>