<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>
        register page
    </title>

    <%@include file='commons/libs_includes.jsp' %>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/register_area.css" />" />
  </head>

  <body>
      <%@include file="commons/header.jsp" %>

      <div class="container" id="register_area_form">
          <div class="row">
              <div class="col-md-12">
                  <div class="well login-box">
                      <form id="register_form" method="post" id="register_area_form_inner">
                          <div class="container">
                              <div class="row">
                                  <div class="col-sm-1">
                                      <label for="username"> Username: </label>
                                  </div>
                                  <div class="col-sm-3">
                                      <input type="text" name="n_username" placeholder="Username" class="form-control" id="username">
                                  </div>
                                  <div class="col-sm-2" id="invalid-username">
                                      Invalid username!
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-1">
                                      <label for="password1">Password: </label>
                                  </div>
                                  <div class="col-sm-3">
                                      <input type="password" name="n_password1" class="form-control" id="password1" placeholder="Password">
                                  </div>
                                  <div class="col-sm-2" id="invalid-password-1">
                                      Invalid password!
                                  </div>
                                  <div class="col-sm-2" id="no-match-password-1">
                                      Passwords do not match!
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-1">
                                      <label for="password2">Password: </label>
                                  </div>
                                  <div class="col-sm-3">
                                      <input type="password" name="n_password2" id="password2" class="form-control" placeholder="Repeat password">
                                  </div>
                                  <div class="col-sm-2" id="invalid-password-2">
                                      Invalid password!
                                  </div>
                                  <div class="col-sm-2" id="no-match-password-2">
                                      Passwords do not match!
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-1">
                                      <label for="name"> Name: </label>
                                  </div>
                                  <div class="col-sm-3">
                                      <input type="text" name="n_name" id="name" class="form-control" placeholder="Type your real name here">
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-1">
                                      <label for="phoneNumber"> Phone: </label>
                                  </div>
                                  <div class="col-sm-3">
                                      <input type="numeric" name="n_phoneNumber" id="phoneNumber" class="form-control"
                                             placeholder="phone number" />
                                  </div>
                                  <div class="col-sm-2" class="invalid-feedback" id="invalid-phone-number">
                                      Invalid phone number!
                                  </div>
                              </div>
                              <div class="row">
                                  <br/>
                              </div>
                          </div>
                          <div id="primary_address_info" class="container">
                              <div class="row">
                                  <div class="col-sm-2">
                                      Country:
                                  </div>
                                  <div class="col-sm-2">
                                      <input type="text" class="form-control" placeholder="Country" name="n_country" id="country" />
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-2">
                                      County:
                                  </div>
                                  <div class="col-sm-2">
                                      <input type="text" class="form-control" placeholder="county" name="n_county" id="county" />
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-2">
                                      Street Name:
                                  </div>
                                  <div class="col-sm-2">
                                      <input type="text" class="form-control" placeholder="streetName"
                                             name="n_streetName" id="streetName" />
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-sm-2">
                                      Street Number:
                                  </div>
                                  <div class="col-sm-2">
                                      <input type="number" class="form-control" placeholder="streetNumber" name="n_streetNumber" id="streetNumber" />
                                  </div>
                                  <div class="col-sm-2" id="invalid-street-number">
                                      Invalid street number!
                                  </div>
                              </div>
                              <div class="row">
                                  <br/>
                              </div>
                          </div>

                          <div class="container">
                              <div class="row">
                                  <div class="col-sm-1">
                                      <button type="button" id="submit_register" class="btn btn-success">
                                          Register
                                      </button>
                                  </div>
                                  <div class="col-sm-2">
                                      <button type="button" id="add_address_info" class="btn btn-light"> + info </button>
                                  </div>
                                  <div class="col-sm-1">
                                      <button type="button" id="hide_address_info" class="btn btn-light"> - hide </button>
                                  </div>
                                  <div class="col-sm-1">
                                    <button
                                            type="button"
                                            id="redirect_to_login_page_from_register"
                                            class="btn btn-primary">
                                        Login
                                    </button>
                                  </div>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>

    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>

    <%@include file='commons/footer.jsp' %>

    <script type="text/javascript" src="<c:url value="/resources/js/register.js" />" ></script>

  </body>
</html>