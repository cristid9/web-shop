<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <title> Login </title>
        <%@include file='commons/libs_includes.jsp' %>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/login_area.css" />" />
    </head>

    <body>
        <%@include file='commons/header.jsp' %>

        <div class="container" id="login_area_form">
            <div class="row">
                <div class="col-md-12">
                    <div class="well login-box">
                        <form id="login_form" method="post">
                            <legend>Login</legend>
                            <div class="form-group">
                                <label for="username">Username</label>
                                <input name="n_username" id="username" placeholder="Username"
                                       type="text" class="form-control" />
                                <div class="col-sm-2" id="empty-username">
                                    The username cannot be empty!
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input name="n_password" id="password" placeholder="Password"
                                       type="password" class="form-control" />
                                <div class="col-sm-2" id="empty-password">
                                    The password cannot be empty!
                                </div>
                            </div>
                            <div class="form-group text-center">
                                <button type="button"
                                        id="redirect_register_from_login"
                                        class="btn btn-success">Register</button>
                                <button id="login_button" type="button" class="btn btn-primary">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="<c:url value="/resources/js/login.js" />"></script>
        <%@include file="commons/footer.jsp" %>
    </body>
</html>
