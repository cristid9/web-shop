<%@ page import="pojo.Order" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>
        <title>
            Orders page
        </title>

        <%@include file='commons/libs_includes.jsp' %>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/orders.css"/>" />
    </head>

    <body>
        <%@include file='commons/header.jsp' %>
        <jsp:useBean id="dateObject" class="java.util.Date" />

        <center>
            <h1> Your Orders </h1>
        </center>

        <table class="table" id="orders_content_area">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Order number</th>
                    <th scope="col">Order status</th>
                    <th scope="col">Date placed</th>
                    <th scope="col">Total</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="item" items="${requestScope.orders}">
                    <tr>
                        <jsp:setProperty name="dateObject" property="time" value="${item.timestamp}" />
                        <th scope="col">${item.id}</th>
                        <th scope="col"> Pending </th>
                        <th scope="col">
                            <fmt:formatDate value="${dateObject}" pattern="dd/MM/yyyy" />
                        </th>
                        <th scope="col">${item.price}</th>
                    </tr>
                </c:forEach>
            </tbody>
        </table>

        <%@include file="commons/footer.jsp" %>
    </body>
</html>