<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <title>
            products
        </title>
        <%@include file="commons/libs_includes.jsp"%>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/products.css" />" />
        <c:choose>
            <c:when test="${requestScope.role == 'admin'}">
                <link rel="stylesheet"
                      type="text/css"
                      href="<c:url value="/resources/css/add_category_option.css" />" />
                <script type="text/javascript" src="<c:url value="/resources/js/add_category.js" />">
                </script>
            </c:when>
        </c:choose>
    </head>

    <body>
        <%@include file="commons/header.jsp"%>
        <script type="text/javascript" src="<c:url value="/resources/js/products.js"/>"></script>
        <script type="text/javascript" src="<c:url value="/resources/js/add_to_cart.js" />"> </script>
        <script type="text/javascript">
            $(document).ready(function () {
               $.get("change_currency", {}, function () {

               }).done(function (data) {
                  $(".product_price").each(function () {
                      var original = $(this).text() + " " + data;
                      $(this).text(original);
                  });
               });

            });
        </script>

        <form id="hidden_form" method="post">
            <input type="hidden" id="hidden_category_marker" name="category_type" />
        </form>

        <div id="products_menu">
            <nav class="navbar navbar-default sidebar" role="navigation" id="products_catgeory_area">
                <div class="container-fluid">
                    <div class="navbar-header">
                    </div>
                    <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
                        <ul id="categoriesArea" class="nav navbar-nav">
                            <c:forEach var="element" items="${requestScope.categories}">
                                <c:choose>
                                    <c:when test="${requestScope.category == element.name}">
                                        <li>
                                            <a class="category_filter_link active_category" href="#">
                                                ${element.name}
                                            </a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li>
                                            <a class="category_filter_link" href="#"> ${element.name} </a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                            ${requestScope.categores}
                            <c:choose>
                                <c:when test="${requestScope.role == 'admin'}">
                                    <div class="row" id="add_new_category_small_form">
                                        <div class="col-md-3">
                                            <button id="add_category_button" type="button" class="btn btn-light">
                                                +
                                            </button>
                                        </div>

                                        <div class="col-md-5">
                                            <input id="add_category_input" class="form-control" type="text" />
                                        </div>
                                    </div>
                                </c:when>
                            </c:choose>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>

        <div id="productsArea" class="container">
            <c:set var="rowCounter" scope="page" value="${0}" />
            <c:forEach var="product" items="${requestScope.products}">
                <c:choose>
                    <c:when test="${rowCounter == 0}">
                        <div class="row">
                    </c:when>
                </c:choose>

                <div class="col-lg-4">

                    <div class="container">
                        <div class="row">
                            <img width="100px"
                                 src="https://www.witneyandmason.com/wp-content/themes/witneyandmason/images/product-placeholder.gif" />
                        </div>
                        <div class="row">
                            <b class="product_name">
                                    ${product.name}
                            </b>
                        </div>
                        <div class="row">
                            <b class="product_price">
                                    ${product.price}
                            </b>
                        </div>
                        <div class="row">
                            <span> Produs de: </span>
                            <b> ${requestScope.producers[product.producer].name} </b>
                        </div>
                        <div class="row">
                            <button type="button" class="btn btn-primary possible_cart_product">
                                add to chart
                            </button>
                            <input type="hidden"
                                   value="${product.id}"
                                   class="product_id" />
                        </div>
                        <div class="row">
                            <br/>
                        </div>
                    </div>
                </div>

                <c:set var="rowCounter" scope="page" value="${rowCounter + 1}" />
                <c:choose>
                    <c:when test="${rowCounter == 3}">
                        </div>
                        <c:set var="rowCounter" scope="page" value="${0}" />
                    </c:when>
                </c:choose>
            </c:forEach>
        </div>

        <%@include file="commons/footer.jsp"%>
    </body>
</html>
