<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <title> Home page </title>
        <%@include file='commons/libs_includes.jsp' %>
    </head>
    <body>
        <%@include file='commons/header.jsp' %>

        <script type="text/javascript" src="<c:url value="/resources/js/add_to_cart.js" />" >
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $.get("change_currency", {}, function () {
                }).done(function (data) {
                    $(".product_price").each(function () {
                        var original = $(this).text() + " " + data;
                        $(this).text(original);
                    });
                });

            });
        </script>
        <center>
            <h2>
                All products in our shop
            </h2>
        </center>

        <div class="container">
            <c:set var="rowCounter" scope="page" value="${0}" />

            <c:forEach var="product" items="${requestScope.products}">
                <c:choose>
                    <c:when test="${rowCounter == 0}">
                        <div class="row">
                    </c:when>
                </c:choose>

                    <div class="col-lg-4">

                       <!-- product placeholder -->
                       <div class="container">
                            <div class="row">
                                <img width="100px"
                                      src="https://www.witneyandmason.com/wp-content/themes/witneyandmason/images/product-placeholder.gif" />
                            </div>
                            <div class="row">
                                <b class="product_name">
                                    ${product.name}
                                </b>
                            </div>
                            <div class="row">
                                <b class="product_price">
                                    ${product.price}
                                </b>
                            </div>
                           <div class="row">
                               <span> Produs de: </span>
                               <b> ${requestScope.producers[product.producer].name} </b>
                           </div>
                            <div class="row">
                                <button type="button" class="btn btn-primary possible_cart_product">
                                    add to chart
                                </button>
                                <input type="hidden"
                                       value="${product.id}"
                                       class="product_id" />
                            </div>
                           <div class="row">
                                <br/>
                            </div>
                       </div>
                       <!-- product placeholder -->
                    </div>

                <c:set var="rowCounter" scope="page" value="${rowCounter + 1}" />
                <c:choose>
                    <c:when test="${rowCounter == 3}">
                        </div>
                        <c:set var="rowCounter" scope="page" value="${0}" />
                    </c:when>
                </c:choose>
            </c:forEach>
        </div>
    </body>
</html>