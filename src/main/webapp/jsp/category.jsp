<%@ page import="pojo.Category" %>
<%@ page import="java.util.List" %>
<html>
    <head>
        <title> Category page </title>
        <%@include file='commons/libs_includes.jsp' %>
        <link rel="stylesheet" type="text/css" href="css/category_area.css" />
    </head>
    <body>
        <%@include file='commons/header.jsp' %>

        <form method="get" action="ordersFiltered" id="category_area">
            <div class="form-group">
                <label for="exampleFormControlSelect1">Example select</label>
                <select class="form-control" id="exampleFormControlSelect1" name="filter">
                    <%
                        List<Category> categories = (List<Category>) request.getAttribute("categories");
                        for (Category category : categories) {
                    %>
                        <option>
                            <%= category.getName() %>
                        </option>
                    <% } %>
                  </select>
            </div>
            <button type="submit" class="btn btn-primary" name="filter_products">
                Filter
            </button>
        </form>
    
        <%@include file="commons/footer.jsp"%>
    </body>
</html>