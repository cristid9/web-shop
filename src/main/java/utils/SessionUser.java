package utils;

import pojo.Cart;
import pojo.User;

public class SessionUser {
    private User currentUser;
    private Cart currentCart;
    private static SessionUser userManager = null;

    private SessionUser() {}

    public static SessionUser getUserManager() {
        return userManager;
    }

    public static void initInstance(User user, Cart cart) {
        if (userManager == null) {
            userManager = new SessionUser();
        }
        userManager.setCurrentCart(cart);
        userManager.setCurrentUser(user);
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public void setCurrentCart(Cart currentCart) {
        this.currentCart = currentCart;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public Cart getCurrentCart() {
        return currentCart;
    }

    public static void invallidateCurrentCart() {
        userManager.setCurrentCart(new Cart());
    }
}
