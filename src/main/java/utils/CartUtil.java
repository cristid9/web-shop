package utils;

import daos.ProductDAO;
import pojo.CartProduct;
import pojo.Product;

import javax.annotation.Resource;

public class CartUtil {

    public static final int DEFAULT_CART_ITEM_QUANTITY = 1;

    @Resource(name = "productDAO")
    private ProductDAO productDAO;

    public CartProduct packCartProduct(int productId) {
        Product product = productDAO.loadProductById(productId);

        CartProduct cartProduct = new CartProduct();
        cartProduct.setPrice(product.getPrice());
        cartProduct.setProductId(product.getId());
        cartProduct.setQuantity(DEFAULT_CART_ITEM_QUANTITY);
        cartProduct.setProduct(product);

        return cartProduct;
    }
}
