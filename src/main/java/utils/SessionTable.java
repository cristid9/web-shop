package utils;

import java.util.HashMap;
import java.util.Map;

public class SessionTable {
    private static Map<String, SessionUser> sessions = new HashMap<>();
    private SessionTable() {}

    public static SessionUser getSession(String sessionId) {
        return sessions.get(sessionId);
    }

    public static void addSession(String sessionId, SessionUser sessionUser)  {
        sessions.put(sessionId, sessionUser);
    }
}
