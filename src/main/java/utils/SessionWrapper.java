package utils;

import pojo.CartProduct;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionWrapper {

    @Resource(name = "cartUtil")
    CartUtil cartUtil;

    public void addToCartOfCurrentSession(HttpServletRequest httpServletRequest, int productId) {
        HttpSession session = httpServletRequest.getSession();
        CartProduct cartProduct = cartUtil.packCartProduct(productId);
        cartProduct.setCartId(SessionUser.getUserManager().getCurrentCart().getId());
        SessionUser.getUserManager().getCurrentCart().addProduct(cartProduct);
    }
}
