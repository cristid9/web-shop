package utils;

import pojo.Cart;
import pojo.CartProduct;

public class GlobalCartManager {
    private static Cart globalCart = new Cart();

    private GlobalCartManager() {}

    public static void addProduct(CartProduct cartProduct) {
        globalCart.addProduct(cartProduct);
    }

    public static Cart getCart() {
        return globalCart;
    }
}
