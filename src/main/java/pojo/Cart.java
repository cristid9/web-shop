package pojo;

import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Table
public class Cart {
    private int id;
    private int totalPrice;
    private List<CartProduct> products;
    private int userId;
    private boolean stillInUse;

    public Cart() {
        this.id = -1;
        products = new ArrayList<>();
    }

    public Cart(int totalPrice, List<CartProduct> products, int userId) {
        this.totalPrice = totalPrice;
        this.products = products;
        this.userId = userId;
    }

    public int getTotalPrice() {
        return totalPrice;
    }

    public List<CartProduct> getProducts() {
        return products;
    }

    public int getId() {
        return id;
    }

    public void setTotalPrice(int totalPrice) {
        this.totalPrice = totalPrice;
    }

    public void setProducts(List<CartProduct> products) {
        this.products = products;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdManually(int id) {
        this.id = id;

        for (CartProduct product : products) {
            product.setCartId(id);
        }
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void addProduct(CartProduct product) {
        if (products != null) {
            products.add(product);
        }
    }

    public boolean isStillInUse() {
        return stillInUse;
    }

    public void setStillInUse(boolean stillInUse) {
        this.stillInUse = stillInUse;
    }

    public int computeTotalPrice() {
        int totalCartPrice = 0;

        for (CartProduct cartProduct : products) {
            totalCartPrice += cartProduct.getPrice();
        }

        return totalCartPrice;
    }

    public boolean isInCart(CartProduct cartProduct) {
        for (CartProduct cartProduct1 : products) {
            if (cartProduct1.getProductId() == cartProduct.getProductId()) {
                return true;
            }
        }
        return false;
    }

    public void removeById(int id) {
        CartProduct cp = null;

        for (CartProduct cartProduct : getProducts()) {
            if (cartProduct.getProductId() == id) {
                cp = cartProduct;
            }
        }
        products.remove(cp);
    }
}
