package pojo;

import javax.persistence.Table;

@Table
public class Producer {
    private int id;
    private String name;
    private int address;

    public Producer(String name, int address) {
        this.name = name;
        this.address = address;
    }

    public Producer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }
}
