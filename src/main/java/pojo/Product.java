package pojo;

import javax.persistence.Table;

@Table
public class Product {
    private int id;
    private String name;
    private int producer;
    private int price;
    private int category;

    public Product() {}

    public Product(String name, int producer, int price) {
        this.name = name;
        this.producer = producer;
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProducer(int producer) {
        this.producer = producer;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getProducer() {
        return producer;
    }

    public int getPrice() {
        return price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
