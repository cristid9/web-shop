package pojo;

// added in the database
public class Address {
    private int id;
    private String streetName;
    private String county;
    private String country;
    private int streetNumber;
    private int apartmentNumber;

    public Address(String streetName, String county, String country, int streetNumber, int apartmentNumber) {
        this.streetName = streetName;
        this.county = county;
        this.country = country;
        this.streetNumber = streetNumber;
        this.apartmentNumber = apartmentNumber;
    }

    public Address() {}

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setStreetNumber(int streetNumber) {
        this.streetNumber = streetNumber;
    }

    public void setApartmentNumber(int apartmentNumber) {
        this.apartmentNumber = apartmentNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getCounty() {
        return county;
    }

    public String getCountry() {
        return country;
    }

    public int getStreetNumber() {
        return streetNumber;
    }

    public int getApartmentNumber() {
        return apartmentNumber;
    }
}
