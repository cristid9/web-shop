package pojo;

import javax.persistence.Table;
import javax.persistence.Transient;

@Table
public class CartProduct {
    private int id;
    private int productId;
    private int price;
    private int quantity;
    @Transient // not relevant from the db perspective
    private Product product; //not meant to be mapped in hibernate
    private int cartId;

    public CartProduct() {}

    public CartProduct(int id, int productId, int price, int quantity) {
        this.id = id;
        this.productId = productId;
        this.price = price;
        this.quantity = quantity;
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Transient
    public int getProductId() {
        return productId;
    }

    @Transient
    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
