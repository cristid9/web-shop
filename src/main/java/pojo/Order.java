package pojo;

import javax.persistence.Table;

@Table
public class Order {
    private int id;
    private int userId;
    private long timestamp;
    private int cartId;
    private int price;

    public Order(int userId, long timestamp, int cartId, int price) {
        this.userId = userId;
        this.timestamp = timestamp;
        this.cartId = cartId;
        this.price = price;
    }

    public Order() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
