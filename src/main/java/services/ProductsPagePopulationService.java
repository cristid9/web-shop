package services;

import pojo.Category;
import pojo.Product;

import java.util.List;

public interface ProductsPagePopulationService {
    List<Product> getInitialPageProductsPopulation();

    List<Category> getInitialCategoriesPopulation();
}
