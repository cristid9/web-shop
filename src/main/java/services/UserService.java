package services;

import pojo.User;

import java.util.List;

public interface UserService {

    boolean registerUser(String name, String password1, String phoneNumber, String username);

    List<User> doListUsers();

    String getUserJSON();

    boolean deleteUser(int targetId);

    void promote(int idVal);

    void downgrade(int idVal);
}
