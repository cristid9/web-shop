package services;

public interface CategoryService {

     boolean addNewCategory(String categoryName);

     String getCategoryString();

     boolean deleteCategoryById(int targetId);
}
