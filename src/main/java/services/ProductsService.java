package services;

public interface ProductsService {
    boolean addProduct(String name, int producer, int price, int category);
}
