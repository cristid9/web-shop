package services;

import pojo.User;

import javax.servlet.http.HttpSession;

public interface LoginManagerService {
    boolean validateLogin(String username, String password);

    void loginUser(String username, HttpSession httpSession);

    void initCart(User user);

    void logout(HttpSession httpSession);

    boolean isLoggedIn(HttpSession httpSession);
}
