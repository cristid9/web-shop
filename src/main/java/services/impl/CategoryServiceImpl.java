package services.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import daos.impl.CategoryDAOImpl;
import pojo.Category;
import services.CategoryService;

import javax.annotation.Resource;
import java.util.List;

public class CategoryServiceImpl implements CategoryService {

    @Resource(name = "categoryDAO")
    CategoryDAOImpl categoryDAO;

    public boolean addNewCategory(String categoryName) {
        Category check = categoryDAO.getCategoryByName(categoryName);

        if (categoryName.length() == 0) {
            return false;
        }

        if (check != null) {
            return false;
        }

        categoryDAO.insertCategory(categoryName);
        return true;
    }

    public String getCategoryString() {
        Gson gson = new GsonBuilder().create();
        List<Category> categories = categoryDAO.loadCategories();
        return gson.toJson(categories);
    }

    public boolean deleteCategoryById(int targetId) {
        categoryDAO.deleteCategory(targetId);
        return true;
    }
}
