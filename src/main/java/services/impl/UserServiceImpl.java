package services.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import daos.UserDAO;

import pojo.User;
import services.UserService;

import javax.annotation.Resource;
import java.util.List;

public class UserServiceImpl implements UserService {

    @Resource(name = "userDAO")
    UserDAO userDAO;

    public boolean registerUser(String name, String password1, String phoneNumber, String username) {

        User check = userDAO.loadUserByUsername(username);

        if (check != null) {
            return false;
        }

        userDAO.insertUser(name, password1, phoneNumber, username);
        return true;
    }

    public List<User> doListUsers() {
        return userDAO.getUsers();
    }

    public String getUserJSON() {
        List<User> users = userDAO.getUsers();
        Gson gson = new GsonBuilder().create();
        return gson.toJson(users);
    }

    public boolean deleteUser(int targetId) {
        userDAO.deleteUser(targetId);
        return true;
    }

    public void promote(int idVal) {
        userDAO.changeRole(idVal, User.ADMIN_ROLE);
    }

    public void downgrade(int idVal) {
        userDAO.changeRole(idVal, User.REGULAR_ROLE);
    }
}
