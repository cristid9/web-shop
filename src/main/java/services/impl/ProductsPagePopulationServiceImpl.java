package services.impl;

import daos.CategoryDAO;
import daos.ProductDAO;
import pojo.Category;
import pojo.Product;
import services.ProductsPagePopulationService;

import javax.annotation.Resource;
import java.util.List;

public class ProductsPagePopulationServiceImpl implements ProductsPagePopulationService {

    @Resource(name = "categoryDAO")
    CategoryDAO categoryDAO;

    @Resource(name = "productDAO")
    ProductDAO productDAO;

    public List<Product> getInitialPageProductsPopulation() {
        List<Category> categories = categoryDAO.loadCategories();
        if (categories != null) {
            Category firstCategory = categories.get(0);
            List<Product> products = productDAO.loadProductsByCategory(firstCategory.getId());

            return products;
        }
        return null;
    }

    public List<Category> getInitialCategoriesPopulation() {
        return categoryDAO.loadCategories();
    }
}
