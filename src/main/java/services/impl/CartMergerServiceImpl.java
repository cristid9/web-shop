package services.impl;

import pojo.Cart;
import pojo.CartProduct;
import services.CartMergerService;

public class CartMergerServiceImpl implements CartMergerService {

    public Cart mergeCarts(Cart globalCart, Cart sessionCart) {

        if (globalCart == null && sessionCart == null) {
            return new Cart();
        }

        if (globalCart == null) {
            return sessionCart;
        }

        if (sessionCart == null) {
            return globalCart;
        }

        for (int i = 0; i < sessionCart.getProducts().size(); ++i) {
            if (globalCart.isInCart(sessionCart.getProducts().get(i))) {
                sessionCart.getProducts().get(i).setQuantity(
                        sessionCart.getProducts().get(i).getQuantity() + 1);
            }
        }

        for (CartProduct cartProduct : globalCart.getProducts()) {
            if (!sessionCart.isInCart(cartProduct)) {
                sessionCart.addProduct(cartProduct);
            }
        }

        return sessionCart;
    }
}
