package services.impl;

import daos.CartDAO;
import daos.CartProductDAO;
import daos.UserDAO;
import daos.connection.SessionManager;
import org.hibernate.Query;
import org.hibernate.Session;
import pojo.Cart;
import pojo.User;
import services.CartMergerService;
import services.LoginManagerService;
import utils.GlobalCartManager;
import utils.SessionUser;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

public class LoginManagerServiceImpl implements LoginManagerService {

    public static final String SESSION_LABEL_NAME = "authenticated";
    public static final String LOGGED_IN_MARKER = "logged_in";

    @Resource(name = "sessionManager")
    SessionManager sessionManager;

    @Resource(name = "userDAO")
    UserDAO userDAO;

    @Resource(name = "cartDAO")
    CartDAO cartDAO;

    @Resource(name = "cartMergerService")
    CartMergerService cartMergerService;

    @Resource(name = "cartProductDAO")
    CartProductDAO cartProductDAO;

    public boolean validateLogin(String username, String password) {
        Session session = sessionManager.getFactory().openSession();
        Query query = session.createQuery("from User where username = '" + username + "'");
        User user = (User) query.uniqueResult();
        if (user != null) {
            return true; // valid login
        }

        return false; // not a valid login
    }

    /* not sure what we are going to do with username field */
    public void loginUser(String username, HttpSession httpSession) {
        User user = userDAO.loadUserByUsername(username);
        initCart(user);
        httpSession.setAttribute(SESSION_LABEL_NAME, true);
    }

    public void initCart(User user) {
        Cart cart = cartDAO.loadCartByUserId(user.getId());

        if (cart == null) {
            cart = new Cart();
            cart.setUserId(user.getId());
        } else {
            cart.setProducts(cartProductDAO.getCartContents(cart.getId()));
        }

        if (GlobalCartManager.getCart().getProducts().size() < 0) {
            // empty cart
            SessionUser.initInstance(user, new Cart());
        } else {
            Cart mergedCart = cartMergerService.mergeCarts(GlobalCartManager.getCart(), cart);
            SessionUser.initInstance(user, mergedCart);
        }
    }

    public void logout(HttpSession session) {
        Cart currentCart = SessionUser.getUserManager().getCurrentCart();

        currentCart.setTotalPrice(currentCart.computeTotalPrice());
        if (currentCart.getId() == -1) {
            currentCart.setIdManually(cartDAO.getLastId());
        }
        currentCart.setUserId(SessionUser.getUserManager().getCurrentUser().getId());

        if (cartDAO.hasUserProducts(SessionUser.getUserManager().getCurrentUser().getId())) {
            cartDAO.updateCachedCart(currentCart);
        } else {
            cartDAO.insertCart(currentCart);
        }

        session.setAttribute(SESSION_LABEL_NAME, false);
        SessionUser.initInstance(null, null);
    }

    public boolean isLoggedIn(HttpSession httpSession) {
        if (httpSession == null) {
            return false;
        }

        Object labelName = httpSession.getAttribute(SESSION_LABEL_NAME);
        if (labelName != null) {
            if ((boolean) labelName) {
                return true;
            }
        }
        return false;
    }
}
