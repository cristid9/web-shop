package services.impl;

import daos.ProductDAO;
import pojo.Product;
import services.ProductsService;

import javax.annotation.Resource;

public class ProductsServiceImpl implements ProductsService  {

    @Resource(name = "productDAO")
    ProductDAO productDAO;

    public boolean addProduct(
            String name,
            int producer,
            int price,
            int category) {

        Product p = new Product();
        p.setName(name);
        p.setCategory(category);
        p.setPrice(price);
        p.setProducer(producer);

        productDAO.insertProduct(p);

        return true;
    }

}
