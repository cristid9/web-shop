package services;

import pojo.Cart;

public interface CartMergerService {

    Cart mergeCarts(Cart globalCart, Cart sessionCart);


}
