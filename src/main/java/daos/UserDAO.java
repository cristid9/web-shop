package daos;

import pojo.User;

import java.util.List;

public interface UserDAO {

    User loadUserByUsername(String username);

    void insertUser(String name, String password1, String phoneNumber, String username);

    List<User> getUsers();

    void deleteUser(int targetId);

    void changeRole(int idVal, String role);

}
