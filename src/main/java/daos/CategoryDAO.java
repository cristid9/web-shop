package daos;

import pojo.Category;

import java.util.List;

public interface CategoryDAO {

    List<Category> loadCategories();

    int getCategoryId(String name);

    Category getCategoryByName(String name);

    void insertCategory(String name);

    void deleteCategory(int targetId);
}
