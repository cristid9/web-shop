package daos;

import pojo.Producer;

import java.util.Map;

public interface ProducerDAO {
    Map<Integer, Producer> getMappedProducers();
}
