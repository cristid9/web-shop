package daos.impl;

import daos.ProducerDAO;
import daos.connection.SessionManager;
import org.hibernate.Session;
import pojo.Producer;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProducerDAOImpl implements ProducerDAO {

    private static final String SELECT_ALL_PRODUCERS = "from Producer";

    @Resource(name = "sessionManager")
    private SessionManager sessionManager;

    public  Map<Integer, Producer> getMappedProducers() {
        Session session = sessionManager.getFactory().openSession();
        List<Producer> producers = (List<Producer>) session.createQuery(SELECT_ALL_PRODUCERS).list();
        session.close();

        Map<Integer, Producer> producersMap = new HashMap<>();
        for (Producer producer : producers) {
            producersMap.put(producer.getId(), producer);
        }

        return producersMap;
    }
}
