package daos.impl;

import daos.ProductDAO;
import daos.connection.SessionManager;
import daos.connection.impl.SessionManagerImpl;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Product;

import javax.annotation.Resource;
import java.util.List;

public class ProductDAOImpl implements ProductDAO {

    private static String SELECT_PRODUCT_BY_ID
            = "from Product where id=%s";
    private static String SELECT_PRODUCTS_BY_CATEGORY
            = "from Product where category=%s";

    @Resource(name = "sessionManager")
    private SessionManager sessionManager;

    public Product loadProductById(int id) {
        String queryString = String.format(SELECT_PRODUCT_BY_ID, id);
        Session session = sessionManager.getFactory().openSession();
        Product product = (Product) session.createQuery(queryString).uniqueResult();
        return product;
    }

    public List<Product> loadProductsByCategory(int categoryId) {
        String qString = String.format(SELECT_PRODUCTS_BY_CATEGORY, categoryId);
        Session session = sessionManager.getFactory().openSession();
        List<Product> products = session.createQuery(qString).list();
        session.close();
        return products;
    }

    public void insertProduct(Product product) {
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.save(product);
        tx.commit();
        session.close();
    }
}
