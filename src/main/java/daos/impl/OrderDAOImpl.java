package daos.impl;

import daos.OrderDAO;
import daos.connection.SessionManager;
import daos.connection.impl.SessionManagerImpl;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Order;

import javax.annotation.Resource;
import java.util.List;

public class OrderDAOImpl implements OrderDAO {

    private static final String SELECT_ORDERS_BY_USER_ID = "from Order where userId=%s";

    @Resource(name = "sessionManager")
    private SessionManager sessionManager;

    public void insertOrder(Order order) {
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.save(order);
        tx.commit();
        session.close();
    }

    public List<Order> extractOrder(int userId) {
        String queryString = String.format(SELECT_ORDERS_BY_USER_ID, userId);
        Session session = sessionManager.getFactory().openSession();
        List<Order> orders = (List<Order>) session.createQuery(queryString).list();
        return orders;
    }
}
