package daos.impl;

import daos.UserDAO;
import daos.connection.SessionManager;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.User;

import javax.annotation.Resource;
import java.util.List;

public class UserDAOImpl implements UserDAO {
    private static final String SELECT_BY_USERNAME_QUERY
            = "from User where username='%s'";

    private static final String SELECT_ALL_USERS
            = "from User";

    private static final String DELETE_USER_BY_ID
            = "delete User where id = %s";


    @Resource(name = "sessionManager")
    private SessionManager sessionManager;

    public UserDAOImpl() { }

    @Override
    public User loadUserByUsername(String username) {
        String queryString = String.format(SELECT_BY_USERNAME_QUERY, username);
        Session session = sessionManager.getFactory().openSession();
        Query query = session.createQuery(queryString);
        User user = (User) query.uniqueResult();
        return user;
    }

    public void insertUser(String name, String password1, String phoneNumber, String username) {
        User user = new User();

        user.setName(name);
        user.setPassword(password1);
        user.setPhoneNumber(phoneNumber);
        user.setUsername(username);
        user.setRole(User.REGULAR_ROLE);

        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.save(user);
        tx.commit();
        session.close();
    }

    public List<User> getUsers() {
        Session session = sessionManager.getFactory().openSession();
        Query query = session.createQuery(SELECT_ALL_USERS);
        List<User> users = query.list();
        session.close();
        return users;
    }

    public void deleteUser(int targetId) {
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        User user = (User) session.load(User.class, targetId);
        session.delete(user);
        tx.commit();
        session.close();
    }

    public void changeRole(int idVal, String role) {
        Session session = sessionManager.getFactory().openSession();
        User user = (User) session.load(User.class, idVal);
        Transaction tx = session.beginTransaction();
        user.setRole(role);
        tx.commit();
        session.saveOrUpdate(user);
        session.close();
    }
}
