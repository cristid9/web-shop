package daos.impl;

import daos.CartDAO;
import daos.CartProductDAO;
import daos.connection.SessionManager;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Cart;
import pojo.CartProduct;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class CartDAOImpl implements CartDAO {

    private static String SELECT_CART_BY_USERY_ID_QUERY_TEMPLATE
            = "from Cart where userId=%s and stillInUse=1";

    @Resource(name = "sessionManager")
    private SessionManager sessionManager;

    @Resource(name = "cartProductDAO")
    private CartProductDAO cartProductDAO;

    public Cart loadCartByUserId(int id) {
        String queryString = String.format(SELECT_CART_BY_USERY_ID_QUERY_TEMPLATE, id);
        Session session = sessionManager.getFactory().openSession();
        Query query = session.createQuery(queryString);
        Cart cart = (Cart) query.uniqueResult();

        if (cart == null) {
            cart = new Cart();
        }
        cart.setProducts(new ArrayList<>());

        return cart;
    }

    public void insertCart(Cart cart) {
        String queryString  =
                String.format("INSERT INTO CART (cart_id, totalPrice, userId, stillInUse) values (%s, %s, %s, %s)",
                cart.getId(), cart.getTotalPrice(), cart.getUserId(), 1);

        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        SQLQuery sqlQuery = session.createSQLQuery(queryString);
        sqlQuery.executeUpdate();
        tx.commit();
        session.close();

        insertProducts(cart.getProducts());
    }

    private void insertProducts(List<CartProduct> cartProducts) {
        for (CartProduct cartProduct : cartProducts) {
            cartProductDAO.insert(cartProduct);
        }
    }

    public void clearUserCarts(Cart cart) {
        if (checkHasEntries()) {
            deleteCart(cart.getId());
            cartProductDAO.eraseUnusedProducts(cart.getId());
        }
    }

    public void deleteCart(int cartId) {
        String queryString = String.format("DELETE FROM CART WHERE cart_id=%s", cartId);
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        SQLQuery sqlQuery = session.createSQLQuery(queryString);
        sqlQuery.executeUpdate();
        tx.commit();
        session.close();
    }

    private boolean checkHasEntries() {
        String queryString = String.format("SELECT COUNT(cart_id) FROM CART");
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        SQLQuery sqlQuery = session.createSQLQuery(queryString);

        if (sqlQuery.list().size() > 0) {
            tx.commit();
            session.close();
            return true;
        }

        tx.commit();
        session.close();
        return false;
    }

    public int getLastId() {
        String queryString = String.format("SELECT MAX(cart_id) + 1 FROM cart");
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();

        int id;
        try {
            id = ((BigInteger) session.createSQLQuery(queryString).uniqueResult()).intValue();
        } catch (Exception e) {
            id = 0;
        }

        tx.commit();
        session.close();

        return id;
    }

    public boolean hasUserProducts(int id) {

        String queryString = String.format("SELECT COUNT(cart_id) FROM cart WHERE userId=%s", id);
        Session session = sessionManager.getFactory().openSession();

        int exists;
        try {
            exists = ((BigInteger) session.createSQLQuery(queryString).uniqueResult()).intValue();
        } catch (Exception e) {
            exists = 0;
        }

        session.close();
        return exists == 1 ? true : false;
    }

    private void deleteCachedProducts(int cartId) {
        String querySring = String.format("DELETE FROM CART_PRODUCT WHERE cart_id=%s", cartId);
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.createSQLQuery(querySring).executeUpdate();
        tx.commit();
        session.close();
    }

    public void updateCachedCart(Cart cart) {
        deleteCachedProducts(cart.getId());
        insertProducts(cart.getProducts());
    }

    public void disableCart(Cart cart) {
        String queryString = String.format("UPDATE CART SET stillInUse=0 WHERE cart_id=%s", cart.getId());
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.createSQLQuery(queryString).executeUpdate();
        tx.commit();
        session.close();
    }
}
