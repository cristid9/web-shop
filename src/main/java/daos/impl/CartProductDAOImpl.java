package daos.impl;

import daos.CartProductDAO;
import daos.ProductDAO;
import daos.connection.SessionManager;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.CartProduct;

import javax.annotation.Resource;
import java.util.List;

public class CartProductDAOImpl implements CartProductDAO {

    @Resource(name = "sessionManager")
    SessionManager sessionManager;

    @Resource(name = "productDAO")
    ProductDAO productDAO;

    public void eraseUnusedProducts(int cartId) {
        String queryString = String.format("DELETE FROM CART_PRODUCT where cart_id=%s", cartId);
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        SQLQuery sqlQuery = session.createSQLQuery(queryString);
        sqlQuery.executeUpdate();
        tx.commit();
        session.close();
    }

    public void insert(CartProduct cartProduct) {
        String queryString = String.format("INSERT INTO cart_product (productId, price, quantity, cart_id) " +
                        "values (%s, %s, %s, %s)", cartProduct.getProductId(), cartProduct.getPrice(),
                        cartProduct.getQuantity(), cartProduct.getCartId());
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        SQLQuery sqlQuery = session.createSQLQuery(queryString);
        sqlQuery.executeUpdate();
        tx.commit();
        session.close();
    }

    private List<CartProduct> getCartContentsInternal(int id) {
        String queryString = String.format("from CartProduct where cartId=%s", id);
        Session session = sessionManager.getFactory().openSession();
        List<CartProduct> cartProducts = session.createQuery(queryString).list();
        session.close();
        return cartProducts;
    }

    public List<CartProduct> getCartContents(int id) {
        List<CartProduct> products = getCartContentsInternal(id);
        for (CartProduct cartProduct : products) {
            cartProduct.setProduct(productDAO.loadProductById(cartProduct.getProductId()));
        }

        return products;
    }
}
