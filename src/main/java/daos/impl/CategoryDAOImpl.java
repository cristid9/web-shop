package daos.impl;

import daos.CategoryDAO;
import daos.connection.SessionManager;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Category;

import javax.annotation.Resource;
import java.util.List;

public class CategoryDAOImpl implements CategoryDAO {

    public static final String queryString = "from Category";
    public static final String SELECT_ID_QUERY = "from Category where name='%s'";

    @Resource(name = "sessionManager")
    private SessionManager sessionManager;

    @Override
    public List<Category> loadCategories() {
        Session session = sessionManager.getFactory().openSession();
        List<Category> categories = session.createQuery(queryString).list();
        session.close();
        return categories;
    }

    @Override
    public int getCategoryId(String name) {
        int id = getCategoryByName(name).getId();
        return id;
    }

    @Override
    public Category getCategoryByName(String name) {
        String qString = String.format(SELECT_ID_QUERY, name);
        Session session = sessionManager.getFactory().openSession();
        Category category =((Category) session.createQuery(qString).uniqueResult());
        session.close();
        return category;
    }

    @Override
    public void insertCategory(String name) {
        Category category = new Category(name);
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.save(category);
        tx.commit();
        session.close();
    }

    @Override
    public void deleteCategory(int targetId) {
        Session session = sessionManager.getFactory().openSession();
        Transaction tx = session.beginTransaction();
        Category user = (Category) session.load(Category.class, targetId);
        session.delete(user);
        tx.commit();
        session.close();
    }
}
