package daos;

import pojo.Cart;

public interface CartDAO {
    Cart loadCartByUserId(int id);

    void insertCart(Cart cart);

    void clearUserCarts(Cart cart);

    void deleteCart(int cartId);

    int getLastId();

    boolean hasUserProducts(int id);

    void updateCachedCart(Cart cart);

    void disableCart(Cart cart);
}
