package daos;

import pojo.Product;

import java.util.List;

public interface ProductDAO {

    Product loadProductById(int id);

    List<Product> loadProductsByCategory(int categoryId);

    void insertProduct(Product product);
}
