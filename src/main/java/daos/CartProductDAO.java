package daos;

import pojo.CartProduct;

import java.util.List;

public interface CartProductDAO {
    void eraseUnusedProducts(int cartId);

    void insert(CartProduct cartProduct);

    List<CartProduct> getCartContents(int id);
}
