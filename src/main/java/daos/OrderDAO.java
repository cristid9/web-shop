package daos;

import pojo.Order;

import java.util.List;

public interface OrderDAO {

    void insertOrder(Order order);

    List<Order> extractOrder(int userId);
}
