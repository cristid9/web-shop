package daos.connection;

import org.hibernate.cfg.Configuration;

public class ConfigurationManager {

    private static Configuration hibConfiguration = null;

    private ConfigurationManager() {}

    public static Configuration getConfiguration() {

        if (hibConfiguration == null) {
            hibConfiguration = new Configuration().addResource("hibernate.cfg.xml").configure();
        }
        return hibConfiguration;
    }
}
