package daos.connection.impl;

import daos.connection.ConfigurationManager;
import daos.connection.SessionManager;
import org.hibernate.SessionFactory;

public class SessionManagerImpl implements SessionManager {

    private SessionManagerImpl() {}

    public SessionFactory getFactory() {
        return ConfigurationManager.getConfiguration().buildSessionFactory();
    }

}
