package daos.connection;

import org.hibernate.SessionFactory;

public interface SessionManager {
    SessionFactory getFactory();
}
