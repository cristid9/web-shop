package youngculture.servlets;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import services.LoginManagerService;
import utils.GlobalCartManager;
import services.impl.LoginManagerServiceImpl;
import utils.SessionUser;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RequestMapping("/get_products_amount")
@RestController
public class ProductsAmountAjaxController {

    @Resource(name = "loginManagerService")
    LoginManagerService loginManagerService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String getAmount() {
        int amountProductsInCart;

        if (loginManagerService.isLoggedIn(getRequest().getSession(false))) {
            amountProductsInCart = SessionUser
                    .getUserManager()
                    .getCurrentCart()
                    .getProducts()
                    .size();
        } else {
            amountProductsInCart = GlobalCartManager.getCart().getProducts().size();
        }

        return String.format("{\"amount\": %s}", amountProductsInCart);
    }

    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest();
    }
}

