package youngculture.servlets;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import services.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("register")
public class RegisterController {

    public static final String REGISTRATION_ERROR = "registration_error";
    public static final String REDIRECT_LOGIN = "redirect:login";
    public static final String N_PHONE_NUMBER = "n_phoneNumber";
    public static final String N_PASSWORD_1 = "n_password1";
    public static final String N_NAME = "n_name";
    public static final String N_USERNAME = "n_username";
    public static final String REGISTER = "register";

    @Resource(name = "userService")
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getRegister() {
        ModelAndView registerView = new ModelAndView(REGISTER);

        return registerView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView postRegister() {

        HttpServletRequest request = getRequest();

//            String sha256hex = Hashing.sha256()
//                    .hashString(originalString, StandardCharsets.UTF_8)
//                    .toString();

        String username = request.getParameter(N_USERNAME);
        String name = request.getParameter(N_NAME);
        String password1 = request.getParameter(N_PASSWORD_1);
        String phoneNumber = request.getParameter(N_PHONE_NUMBER);

        if (userService.registerUser(name, password1, phoneNumber, username)) {
            return new ModelAndView(REDIRECT_LOGIN);
        }

        return new ModelAndView(REGISTRATION_ERROR);
    }

    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest();
    }
}
