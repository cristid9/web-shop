package youngculture.servlets;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import pojo.User;
import services.LoginManagerService;
import services.impl.LoginManagerServiceImpl;
import utils.SessionUser;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("admin_page")
public class AdminPageController {

    public static final String LOGGED_IN = "logged_in";
    public static final String ADMIN_PAGE = "admin_page";
    public static final String ACCESS_DENIED = "access_denied";

    @Resource(name = "loginManagerService")
    LoginManagerService loginManagerService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getAdminPage() {

        if (loginManagerService.isLoggedIn(getRequest().getSession(false))) {
            if (SessionUser.getUserManager().getCurrentUser().getRole().equals(User.ADMIN_ROLE)) {
                ModelAndView adminPage = new ModelAndView(ADMIN_PAGE);
                adminPage.addObject(LOGGED_IN, true);

                return adminPage;
            }
        }

        ModelAndView deniedView = new ModelAndView(ACCESS_DENIED);
        return deniedView;
    }

    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest();
    }
}
