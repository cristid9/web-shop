package youngculture.servlets;

import daos.ProducerDAO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import pojo.Cart;
import pojo.Producer;
import services.LoginManagerService;
import utils.GlobalCartManager;
import services.impl.LoginManagerServiceImpl;
import utils.SessionUser;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@Controller
@RequestMapping("cart")
public class CartPageController {

    public static final String CURRENT_CART_MARKER = "current_cart";
    public static final String PRODUCERS = "producers";
    public static final String CART = "cart";

    @Resource(name = "producerDAO")
    ProducerDAO producerDAO;

    @Resource(name = "loginManagerService")
    LoginManagerService loginManagerService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getCart() {
        ModelAndView cartView = new ModelAndView(CART);

        HttpSession session = getRequest().getSession(false);

        Map<Integer, Producer> producers = producerDAO.getMappedProducers();
        cartView.addObject(PRODUCERS, producers);

        if (loginManagerService.isLoggedIn(session)) {
            Cart currentCart = SessionUser.getUserManager().getCurrentCart();
            cartView.addObject(CURRENT_CART_MARKER, currentCart);
            cartView.addObject(LoginManagerServiceImpl.LOGGED_IN_MARKER, true);
        } else {
            cartView.addObject(LoginManagerServiceImpl.LOGGED_IN_MARKER, false);
            cartView.addObject(CURRENT_CART_MARKER, GlobalCartManager.getCart());
        }

        return cartView;
    }

    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attr
                = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest();
    }
}
