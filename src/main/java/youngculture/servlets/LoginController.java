package youngculture.servlets;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import pojo.User;
import services.LoginManagerService;
import utils.SessionUser;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("login")
public class LoginController {

    public static final String SPECIAL_BUTTON = "special_button";
    public static final String N_USERNAME = "n_username";
    public static final String N_PASSWORD = "n_password";
    public static final String LOGIN = "login";
    public static final String REDIRECT_HOME = "redirect:home";
    public static final String REDIRECT_ADMIN_PAGE = "redirect:admin_page";

    @Resource(name = "loginManagerService")
    LoginManagerService loginManagerService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getLogin() {

        ModelAndView loginView = new ModelAndView(LOGIN);
        loginView.addObject(SPECIAL_BUTTON, true);

        return loginView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView postLogin() {
        HttpServletRequest request = getRequest();

        String username = request.getParameter(N_USERNAME);
        String password = request.getParameter(N_PASSWORD);

        if (loginManagerService.validateLogin(username, password)) {
            loginManagerService.loginUser(username, getRequest().getSession(false));

            if (SessionUser.getUserManager().getCurrentUser().getRole().equals(User.ADMIN_ROLE)) {
                System.out.println("We've logged in an admin user");
                return new ModelAndView(REDIRECT_ADMIN_PAGE);
            }
        }

        return new ModelAndView(REDIRECT_HOME);
    }

    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest();
    }
}
