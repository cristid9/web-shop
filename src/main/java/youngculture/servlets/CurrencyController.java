package youngculture.servlets;


import org.springframework.web.bind.annotation.*;

@RequestMapping("change_currency")
@RestController
public class CurrencyController {

    public static final String OK = "OK";
    public static String currency = "RON";

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String changeCurrency(@RequestParam("currency") String newCurrency) {
        currency = newCurrency;

        System.out.println("CHanged currency");

        return OK;
    }


    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String getCurrency() {
        return currency;
    }
}
