package youngculture.servlets;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import services.UserService;

import javax.annotation.Resource;

@RequestMapping("list_users")
@RestController
public class ListUsersAjaxController {

    @Resource(name = "userService")
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String doListUsers() {
        String userList = userService.getUserJSON();
        return userList;
    }

}
