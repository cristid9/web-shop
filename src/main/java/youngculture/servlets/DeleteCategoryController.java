package youngculture.servlets;

import org.springframework.web.bind.annotation.*;
import services.CategoryService;

import javax.annotation.Resource;

@RequestMapping("delete_category")
@RestController
public class DeleteCategoryController {

    public static final String OK = "OK";

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String deleteCategory(@RequestParam("id") String id) {
        int idVal = Integer.valueOf(id);
        categoryService.deleteCategoryById(idVal);
        return OK;
    }
}
