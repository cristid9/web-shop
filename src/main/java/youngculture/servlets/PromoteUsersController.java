package youngculture.servlets;

import org.springframework.web.bind.annotation.*;
import services.UserService;

import javax.annotation.Resource;

@RequestMapping("promote_user")
@RestController
public class PromoteUsersController {

    public static final String OK = "OK";

    @Resource(name = "userService")
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String PromoteUser(@RequestParam("id") String id) {
        int idVal = Integer.valueOf(id);
        userService.promote(idVal);
        return OK;
    }
}
