package youngculture.servlets;

import daos.CartDAO;
import daos.ProducerDAO;
import daos.connection.SessionManager;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import pojo.Producer;
import services.LoginManagerService;
import services.impl.LoginManagerServiceImpl;
import utils.SessionUser;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/home")
public class HomepageController {

    public static final String PRODUCTS = "products";
    public static final String PRODUCERS = "producers";
    public static final String HOMEPAGE = "homepage";

    @Resource(name = "sessionManager")
    SessionManager sessionManager;

    @Resource(name = "producerDAO")
    ProducerDAO producerDAO;

    @Resource(name = "cartDAO")
    CartDAO cartDAO;

    @Resource(name = "loginManagerService")
    LoginManagerService loginManagerService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getHome() {

        ModelAndView homeView = new ModelAndView(HOMEPAGE);

        // TODO: refactor
        Session session = sessionManager.getFactory().openSession();
        String hql = "from Product";
        Query query = session.createQuery(hql);
        List results = query.list();
        session.close();
        Map<Integer, Producer> producers = producerDAO.getMappedProducers();

        homeView.addObject(PRODUCTS, results);
        homeView.addObject(PRODUCERS, producers);

        if (loginManagerService.isLoggedIn(getRequest().getSession(false))) {
            homeView.addObject(LoginManagerServiceImpl.LOGGED_IN_MARKER, true);
            cartDAO.clearUserCarts(SessionUser.getUserManager().getCurrentCart());
        } else {
            homeView.addObject(LoginManagerServiceImpl.LOGGED_IN_MARKER, false);
        }

        return homeView;
    }

    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest();
    }
}
