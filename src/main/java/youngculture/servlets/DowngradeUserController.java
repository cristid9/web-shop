package youngculture.servlets;

import org.springframework.web.bind.annotation.*;
import services.UserService;

import javax.annotation.Resource;

@RequestMapping("downgrade_user")
@RestController
public class DowngradeUserController {

    public static final String OK = "OK";

    @Resource(name = "userService")
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String downgradeUser(@RequestParam("id") String id) {
        int idVal = Integer.valueOf(id);
        userService.downgrade(idVal);
        return OK;
    }
}
