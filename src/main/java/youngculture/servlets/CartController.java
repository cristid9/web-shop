package youngculture.servlets;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import services.LoginManagerService;
import utils.CartUtil;
import utils.GlobalCartManager;
import utils.SessionWrapper;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/add_to_cart")
public class CartController {

    public static final String PRODUCT_ID = "product_id";
    public static final String CALL_TO_CART_AJAX_CONTROLLER_DETECTED = "CALL TO CART AJAX CONTROLLER DETECTED";
    public static final String EMPTY_RESPONSE = "";

    @Resource(name = "loginManagerService")
    LoginManagerService loginManagerService;

    @Resource(name = "cartUtil")
    CartUtil cartUtil;

    @Resource(name = "sessionWrapper")
    SessionWrapper sessionWrapper;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String addToCart() {
        HttpServletRequest request = getRequest();

        int productId = Integer.parseInt(request.getParameter(PRODUCT_ID));

        if (loginManagerService.isLoggedIn(getRequest().getSession(false))) {
            sessionWrapper.addToCartOfCurrentSession(request, productId);
        } else {
            GlobalCartManager.addProduct(cartUtil.packCartProduct(productId));
        }
        System.out.println(CALL_TO_CART_AJAX_CONTROLLER_DETECTED);

        return EMPTY_RESPONSE;
    }

    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest();
    }
}
