package youngculture.servlets;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import services.CategoryService;

import javax.annotation.Resource;

@RequestMapping("list_categories")
@RestController
public class ListCategoriesAjaxController {

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String doListCategories() {
        String categoriesJSON = categoryService.getCategoryString();
        return categoriesJSON;
    }

}
