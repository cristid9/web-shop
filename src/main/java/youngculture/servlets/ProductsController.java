package youngculture.servlets;

import daos.CategoryDAO;
import daos.ProducerDAO;
import daos.ProductDAO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import pojo.Category;
import pojo.Producer;
import pojo.Product;
import pojo.User;
import services.LoginManagerService;
import services.ProductsPagePopulationService;
import utils.SessionUser;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("products")
public class ProductsController {

    public static final String PRODUCTS = "products";
    public static final String CATEGORY_TYPE = "category_type";
    public static final String PRODUCERS = "producers";
    public static final String CATEGORY = "category";
    public static final String CATEGORIES = "categories";
    public static final String ROLE = "role";
    public static final String LOGGED_IN = "logged_in";

    @Resource(name = "productsPagePopulationService")
    ProductsPagePopulationService productsPagePopulationService;

    @Resource(name = "producerDAO")
    ProducerDAO producerDAO;

    @Resource(name = "productDAO")
    ProductDAO productDAO;

    @Resource(name = "categoryDAO")
    CategoryDAO categoryDAO;

    @Resource(name = "loginManagerService")
    LoginManagerService loginManagerService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getProducts() {
        HttpServletRequest request = getRequest();
        ModelAndView productsView = new ModelAndView(PRODUCTS);

        List<Category> categories = productsPagePopulationService.getInitialCategoriesPopulation();
        Map<Integer, Producer> producers = producerDAO.getMappedProducers();
        request.setAttribute(PRODUCERS, producers);

        String category = (String) request.getAttribute(CATEGORY);
        buildModel(productsView, category, categories);

        return productsView;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView postProducts() {

        ModelAndView productsView = new ModelAndView(PRODUCTS);
        HttpServletRequest request = getRequest();
        String category = request.getParameter(CATEGORY_TYPE);
        System.out.println("Category: " + category);

        List<Category> categories = productsPagePopulationService.getInitialCategoriesPopulation();
        Map<Integer, Producer> producers = producerDAO.getMappedProducers();
        request.setAttribute(PRODUCERS, producers);

        return buildModel(productsView, category, categories);
    }

    private ModelAndView buildModel(ModelAndView productsView, String category, List<Category> categories) {
        List<Product> products;
        if (category == null) {
            products = productsPagePopulationService.getInitialPageProductsPopulation();
            productsView.addObject(CATEGORY, categories.get(0).getName());
            productsView.addObject(PRODUCTS, products);
            productsView.addObject(CATEGORIES, categories);
        } else {
            products = productDAO.loadProductsByCategory(categoryDAO.getCategoryId(category));
            productsView.addObject(CATEGORY, category);
            productsView.addObject(PRODUCTS, products);
            productsView.addObject(CATEGORIES, categories);
        }

        if (loginManagerService.isLoggedIn(getRequest().getSession(false))) {
            if (SessionUser.getUserManager().getCurrentUser().getRole().equals(User.ADMIN_ROLE)) {
                productsView.addObject(ROLE, User.ADMIN_ROLE);
                productsView.addObject(LOGGED_IN, true);
            } else {
                productsView.addObject(ROLE, User.REGULAR_ROLE);
                productsView.addObject(LOGGED_IN, false);
            }
        }

        return productsView;
    }

    private static HttpServletRequest getRequest() {
        ServletRequestAttributes attr = (ServletRequestAttributes)
                RequestContextHolder.currentRequestAttributes();
        return attr.getRequest();
    }
}
