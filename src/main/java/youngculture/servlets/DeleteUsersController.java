package youngculture.servlets;

import org.springframework.web.bind.annotation.*;
import services.UserService;

import javax.annotation.Resource;

@RestController
@RequestMapping("delete_user")
public class DeleteUsersController {

    public static final String OK = "OK";

    @Resource(name = "userService")
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String deleteUser(@RequestParam("id") String id) {
        int targetId = Integer.valueOf(id);
        userService.deleteUser(targetId);
        return OK;
    }
}
