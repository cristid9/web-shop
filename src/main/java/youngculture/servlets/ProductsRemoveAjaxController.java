package youngculture.servlets;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import services.LoginManagerService;
import utils.GlobalCartManager;
import utils.SessionUser;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/remove_product")
public class ProductsRemoveAjaxController {

    public static final String TARGET_ID = "target_id";
    public static final String REMOVE_PRODUCT_CALL_TO_PRODUCTS_REMOVE_CONTROLLER_DETECTED
            = "[REMOVE PRODUCT] CALL TO PRODUCTS REMOVE CONTROLLER DETECTED";

    @Resource(name = "loginManagerService")
    LoginManagerService loginManagerService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public void doPost() {
        HttpServletRequest request = getRequest();
        int removeId = Integer.parseInt(request.getParameter(TARGET_ID));

        if (loginManagerService.isLoggedIn(getRequest().getSession(false))) {
            SessionUser.getUserManager().getCurrentCart().removeById(removeId);
        } else {
            GlobalCartManager.getCart().removeById(removeId);
        }

        System.out.println(REMOVE_PRODUCT_CALL_TO_PRODUCTS_REMOVE_CONTROLLER_DETECTED);
    }

    public static HttpServletRequest getRequest() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest();
    }
}
