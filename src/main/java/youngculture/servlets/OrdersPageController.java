package youngculture.servlets;

import daos.impl.OrderDAOImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;
import pojo.Order;
import services.LoginManagerService;
import utils.SessionUser;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("orders")
public class OrdersPageController {

    public static final String ORDERS = "orders";
    public static final String ORDERS1 = "orders";
    public static final String LOGGED_IN = "logged_in";

    @Resource(name = "orderDAO")
    OrderDAOImpl orderDAO;

    @Resource(name = "loginManagerService")
    LoginManagerService loginManagerService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getOrders() {

        ModelAndView ordersView = new ModelAndView(ORDERS);

        int userId = SessionUser.getUserManager().getCurrentUser().getId();
        List<Order> orders = orderDAO.extractOrder(userId);
        ordersView.addObject(ORDERS1, orders);

        if (loginManagerService.isLoggedIn(getRequest().getSession(false))) {
            ordersView.addObject(LOGGED_IN, true);
        } else {
            ordersView.addObject(LOGGED_IN, false);
        }

        return ordersView;
    }

    private static HttpServletRequest getRequest() {
        ServletRequestAttributes attr = (ServletRequestAttributes)
                RequestContextHolder.currentRequestAttributes();
        return attr.getRequest();
    }

}
