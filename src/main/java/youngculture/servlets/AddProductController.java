package youngculture.servlets;

import org.springframework.web.bind.annotation.*;
import services.ProductsService;
import services.impl.ProductsServiceImpl;

import javax.annotation.Resource;

@RequestMapping("add_product")
@RestController
public class AddProductController {

    public static final String OK = "OK";

    @Resource(name = "productService")
    private ProductsService productsService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String addProdudct(
            @RequestParam("name") String name,
            @RequestParam("producer") String producer,
            @RequestParam("price") String price,
            @RequestParam("category") String category) {

        int priceVal = Integer.valueOf(price);
        int categoryVal = Integer.valueOf(category);
        int producerVal = Integer.valueOf(producer);

        productsService.addProduct(name, producerVal, priceVal, categoryVal);

        return OK;
    }
}
