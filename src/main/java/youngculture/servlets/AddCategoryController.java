package youngculture.servlets;

import org.springframework.web.bind.annotation.*;
import services.CategoryService;

import javax.annotation.Resource;

@RequestMapping("/add_category")
@RestController
public class AddCategoryController {
    public static final String SUCCESS = "OK";
    public static final String FAILURE = "FAIL";

    @Resource(name = "categoryService")
    private CategoryService categoryService;

    @ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public String addCategory(@RequestParam("name") String name) {
        if (categoryService.addNewCategory(name)) {
            return String.format("{\"status\": \"%s\"}", SUCCESS);
        }
        return String.format("{\"status\":\"%s\"}", FAILURE);
    }
}
