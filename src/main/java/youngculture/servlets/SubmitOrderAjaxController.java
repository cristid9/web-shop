package youngculture.servlets;

import daos.CartDAO;
import daos.OrderDAO;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import pojo.Cart;
import pojo.Order;

import services.LoginManagerService;
import utils.SessionUser;

import javax.annotation.Resource;

@RestController
@RequestMapping("/submit_order")
public class SubmitOrderAjaxController {

    public static final String EMPTY_RESPONSE = "";

    @Resource(name = "cartDAO")
    CartDAO cartDAO;

    @Resource(name = "orderDAO")
    OrderDAO orderDAO;

    @Resource(name = "loginManagerService")
    LoginManagerService loginManagerService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String submitOrder() {

        Cart cart = SessionUser.getUserManager().getCurrentCart();
        cart.setStillInUse(false);

        // invalidate current cart

        SessionUser.getUserManager().invallidateCurrentCart();

        cartDAO.insertCart(cart);
        loginManagerService.initCart(SessionUser.getUserManager().getCurrentUser());

        // needs an order DAO
        Order order = new Order();
        order.setUserId(SessionUser.getUserManager().getCurrentUser().getId());
        order.setCartId(cart.getId());
        order.setTimestamp(System.currentTimeMillis());
        order.setPrice(cart.computeTotalPrice());

        orderDAO.insertOrder(order);

        return EMPTY_RESPONSE;
    }
}
