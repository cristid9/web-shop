CREATE TABLE PRODUCT (
    id number(10) not null,
    producer number(10) not null,
    price number(10) not null
);
--
ALTER TABLE PRODUCT ADD (
    CONSTRAINT p_pk PRIMARY KEY (ID));
--
CREATE SEQUENCE p_seq START WITH 1;
--
CREATE OR REPLACE TRIGGER p_bir
BEFORE INSERT ON product
FOR EACH ROW
BEGIN
    SELECT p_seq.NEXTVAL
    INTO :new.id
    FROM dual;
END;
/