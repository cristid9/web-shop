-- ================================================
-- ===== populate the database with producers =====
-- ================================================

-- Phones
insert into producer(name, address) values('Huawei', 1); -- using dummy address
insert into producer(name, address) values('Samsung', 1); -- using dummy address
insert into producer(name, address) values('LG', 1); -- using dummy address

-- Shoes
insert into producer(name, address) values('Puma', 1);
insert into producer(name, address) values('Nike', 1);
insert into producer(name, address) values('Asics', 1);

-- T-shirts
insert into producer(name, address) values('Zara', 1);
insert into producer(name, address) values('Pull & Bear', 1);
insert into producer(name, address) values('Terranova', 1);

-- ======================
-- ===== Categories =====
-- ======================
insert into category(name) values ('T-shirt');
insert into category(name) values ('Shoes');
insert into category(name) values ('Phones');

-- ==================
-- ==== Products ====
-- ==================

-- == Telefoane ==
-- samsung
insert into product(name, producer, price, category) values ('Galaxy s8', 2, 2500, 5);
insert into product(name, producer, price, category) values ('Galaxy A5', 2, 1500, 5);

-- huawei
insert into product(name, producer, price, category) values ('P9 ', 1, 1800, 5);
insert into product(name, producer, price, category) values ('P8', 1, 1700, 5);

-- LG
insert into product(name, producer, price, category) values ('TX780 ', 3, 150, 5);
insert into product(name, producer, price, category) values ('TX770', 3, 120, 5);

-- == Shoes ==
-- puma
insert into product(name, producer, price, category) values ('Pro Sport 2 ', 4, 250, 4);
insert into product(name, producer, price, category) values ('Casual ', 4, 200, 4);

-- Nike
insert into product(name, producer, price, category) values ('Nike MD Runner 46', 5, 250, 4);
insert into product(name, producer, price, category) values ('Nike MD Runner 44 ', 5, 200, 4);

-- Asics
insert into product(name, producer, price, category) values ('Urban Sneakers', 6, 300, 4);
insert into product(name, producer, price, category) values ('Tenis shoes', 6, 210, 4);

-- == T-shirts ==
-- Zara
insert into product(name, producer, price, category) values ('Casual Z1', 7, 30, 3);
insert into product(name, producer, price, category) values ('Casual Z2', 7, 100, 3);

-- Pull & Bear
insert into product(name, producer, price, category) values ('Casual PB1', 8, 50, 3);
insert into product(name, producer, price, category) values ('Casual PB2', 8, 60, 3);

-- Terranova
insert into product(name, producer, price, category) values ('Casual T1', 9, 50, 3);
insert into product(name, producer, price, category) values ('Casual T2', 9, 60, 3);

-- Fructe
insert into product(name, producer, price, category) values ('Mango', 0, 5, 1);
insert into product(name, producer, price, category) values ('Papaya', 0, 6, 1);

-- Legume
insert into product(name, producer, price, category) values ('Marar', 0, 5, 2);
insert into product(name, producer, price, category) values ('Patrunjel', 0, 6, 2);
