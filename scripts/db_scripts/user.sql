CREATE TABLE USER (
    id number(10) not null,
    name varchar2(200) not null,
    username varchar2(200) not null,
    password varchar2(200) not null,
    phone_number varchar2(200) not null
);
--
ALTER TABLE SHOP_USER ADD (
    CONSTRAINT su_pk PRIMARY KEY (ID));
--
CREATE SEQUENCE su_seq START WITH 1;
--
CREATE OR REPLACE TRIGGER su_bir
BEFORE INSERT ON shop_user
FOR EACH ROW

BEGIN
    SELECT su_seq.NEXTVAL
    INTO :new.id
    FROM dual;
END;
/
------------------------------------------------------------------------------------------------------------------------
